package info.owczarek.ssr.kalambury.klient.gui;

import javax.swing.*;

public class PlayersList extends JList<String> {
	private DefaultListModel<String> lmUzytkownicy;
	private int numberOfUsers;

	public PlayersList() {
		createElements();
		deployElements();
		numberOfUsers = 0;
	}

	private void createElements() {
		lmUzytkownicy = new DefaultListModel<String>();
		setPrototypeCellValue("Jan z Warszawy");
	}

	private void deployElements() {
		setModel(lmUzytkownicy);
	}

	public void newUser(String userName) {
		lmUzytkownicy.addElement(userName);
		numberOfUsers++;
	}

	public void removeUser(String userName) {
		lmUzytkownicy.removeElement(userName);
		numberOfUsers--;
	}
	
	public int getNumberOfPlayers() {
		return numberOfUsers;
	}
}
