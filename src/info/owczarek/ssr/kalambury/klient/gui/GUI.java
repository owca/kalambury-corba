package info.owczarek.ssr.kalambury.klient.gui;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import org.apache.logging.log4j.*;
import info.owczarek.ssr.kalambury.klient.*;
import info.owczarek.ssr.kalambury.klient.gui.utils.MouseTrack;

import javax.swing.*;

public class GUI {
	private JFrame frame;
	private JPanel mainPanel;
	private ConnectionPanel connectionPanel;
	private PaintPanel paint;
	private JPanel bottomPanel;
	private PlayersList playersList;
	private GuessPanel guessPanel;
	private Controler controler;
	private static Logger logger = LogManager.getLogger("GUI");

	public GUI() {
		createComponents();
		deployComponents();
		connectListeners();
		logger.info("GUI created");
	}

	private void createComponents() {
		frame = new JFrame();
		mainPanel = new JPanel();
		connectionPanel = new ConnectionPanel(this);
		paint = new PaintPanel(this);
		bottomPanel = new JPanel();
		guessPanel = new GuessPanel(this);
		playersList = new PlayersList();
	}

	private void deployComponents() {
		frame.setTitle(Configuration.TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainPanel.setLayout(new BorderLayout());

		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
		bottomPanel.add(guessPanel);
		bottomPanel.add(new JScrollPane(playersList));

		mainPanel.add(paint, BorderLayout.CENTER);
		mainPanel.add(bottomPanel, BorderLayout.PAGE_END);

		frame.add(connectionPanel);
		frame.pack();
	}

	private void connectListeners() {
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				controler.logOut();
				logger.info("Logging out invoked by closing window");
			}
		});
	}

	public void connectControler(Controler controler) {
		this.controler = controler;
	}

	/**
	 * Shows window if controller is connected
	 */
	public void show() {
		if (controler != null) {
			frame.setVisible(true);
		}
	}

	/**
	 * Tries to log in passing values to underlying controler
	 * 
	 * @param host
	 *            where server is
	 * @param port
	 *            on which port it works?
	 * @param userName
	 *            user name
	 * @return
	 */
	public boolean logIn(String host, int port, String userName) {
		return controler.logIn(host, port, userName);
	}

	public void aShowPaintPanel() {
		guessPanel.block();
		paint.block();
		frame.remove(connectionPanel);
		frame.add(mainPanel);
		frame.setTitle(Configuration.TITLE + ": "
				+ connectionPanel.getUserName());
		frame.setSize(Configuration.WIDTH, Configuration.HEIGHT);
	}

	/**
	 * ClientImpl notifies that new user joined the game. We have to add him and
	 * print message
	 * 
	 * @param newUserName
	 *            new user's name
	 */
	public void newUserJoined(String newUserName) {
		playersList.newUser(newUserName);
		guessPanel.addMessage("Użytkownik " + newUserName + " dołączył do gry");
	}

	/**
	 * ClientImpl notifies that some user left the game so we remove him and
	 * print message
	 * 
	 * @param oldUser
	 */
	public void userLeft(String oldUser) {
		playersList.removeUser(oldUser);
		guessPanel.addMessage("Użytkownik " + oldUser + " opuścił grę");
	}

	/**
	 * Player tries to guess the correct phrase.
	 * 
	 * @param guess
	 *            phase which is supposed to be correct
	 */
	public void aTryToGuess(String guess) {
		controler.aTryToGuess(guess);
	}

	/**
	 * The current player is the admin so it needs to have admin panel available
	 * to set the number of turns
	 */
	public void youreTheAdmin() {
		guessPanel.showAdminPanel();
	}

	/**
	 * Admin started game
	 * 
	 * @param numberOfTurns
	 *            how many turns it will take
	 */
	public void aStartGame(int numberOfTurns) {
		controler.aStartGame(numberOfTurns);
	}

	/**
	 * Another player starts to draw
	 * 
	 * @param categoryName
	 *            category of phrase
	 */
	public void newDrawing(String categoryName, String drawer) {
		guessPanel.unblock();
		guessPanel.receiveCategoryName(categoryName);
		guessPanel.addMessage("Rysuje " + drawer);
		paint.block();
	}

	/**
	 * This player is going to draw
	 * 
	 * @param phrase
	 *            what to draw
	 */
	public void startDrawing(String phrase) {
		guessPanel.block();
		paint.unblock();
		guessPanel.receivePhraseToDraw(phrase);
	}

	/**
	 * The drawing player released mouse button thus sending the path to others
	 * 
	 * @param mt
	 *            track to be sent
	 */
	public void aSendMouseTrack(MouseTrack mt) {
		controler.aSendMouseTrack(mt);
	}

	/**
	 * The current player received path drawn by sb else and it needs to be
	 * drawn
	 * 
	 * @param mt
	 *            track to draw
	 */
	public void receiveDrawingPath(MouseTrack mt) {
		paint.receiveDrawingPath(mt);
	}

	/**
	 * 
	 * @param player
	 * @param guess
	 * @param wasSuccessful
	 */
	public void guessTrial(String player, String guess, boolean wasSuccessful) {
		if (!wasSuccessful) {
			guessPanel
					.addMessage("Użytkownik " + player + " zgaduje: " + guess);
		} else {
			guessPanel.addMessage("Użytkownik " + player
					+ " podał właściwe hasło: " + guess);
		}
	}

	/**
	 * Returns number of players in game. Administrator needs it to be > 1 to
	 * start the game
	 * 
	 * @return how many players are in game
	 */
	public int getNumberOfPlayers() {
		return playersList.getNumberOfPlayers();
	}
}
