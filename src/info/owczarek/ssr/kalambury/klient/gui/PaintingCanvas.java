package info.owczarek.ssr.kalambury.klient.gui;

import info.owczarek.ssr.kalambury.klient.gui.utils.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.*;
import org.apache.logging.log4j.*;

/**
 * This class is was thought to provide drawing functionality You can set the
 * colour and start drawing using your mouse
 */
public class PaintingCanvas extends JPanel implements MouseMotionListener,
		MouseListener, ColorSwitching {
	private static final int WIDTH = 640;
	private static final int HEIGHT = 480;
	private BufferedImage canvas;
	private int colourRGB;
	private Color colour;
	private Point previousPoint;
	private boolean drawingEnabled;
	private Set<PaintListener> paintListeners;
	private static Logger logger = LogManager.getLogger("Painting Canvas");

	private MouseTrack mouseTrack;

	public PaintingCanvas() {
		Dimension size = new Dimension(WIDTH, HEIGHT);
		setSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setColour(0xffffff);
		canvas = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		paintListeners = new HashSet<>();
		connectListeners();

		drawingEnabled = true;
	}

	private void connectListeners() {
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void addPaintListener(PaintListener listener) {
		paintListeners.add(listener);
	}

	public void removePaintListener(PaintListener listener) {
		paintListeners.remove(listener);
	}

	private void firePaintListeners() {

		Iterator<PaintListener> listenersIterator = paintListeners.iterator();
		while (listenersIterator.hasNext()) {
			PaintListener listener = listenersIterator.next();
			listener.aReceiveMouseTrack(mouseTrack);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(canvas, 0, 0, WIDTH, HEIGHT, null);
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		mouseTrack = new MouseTrack(colourRGB);
		previousPoint = new Point(e.getX(), e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		logger.debug("Mouse released");
		firePaintListeners();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point currentCoodrinates = new Point(e.getX(), e.getY());
		mouseTrack.addCoodrinate(currentCoodrinates);
		drawLine(e.getX(), e.getY());
		previousPoint = new Point(e.getX(), e.getY());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void setColour(int rgb) {
		colourRGB = rgb;
		colour = new Color(rgb);
	}

	/**
	 * Draws a line from the point the mouse was dragged from to the current
	 * point
	 * 
	 * @param x
	 *            current coordinate x
	 * @param y
	 *            current coordinate y
	 * @param rgb
	 */
	public void drawLine(int x, int y) {
		if (drawingEnabled) {
			Graphics2D g2d = canvas.createGraphics();
			g2d.setColor(colour);
			g2d.drawLine(previousPoint.getX(), previousPoint.getY(), x, y);
			repaint();
		}
	}

	/**
	 * This method draws some MouseTrack. It can be given from another client
	 * 
	 * @param mt
	 *            MouseTrack to draw
	 */
	public void drawMouseTrack(MouseTrack mt) {
		Graphics2D g2d = canvas.createGraphics();
		g2d.setColor(mt.getColor());

		java.util.List<Point> points = mt.getCoordinates();
		for (int x = 0; x < points.size() - 1; x++) {
			int x1 = points.get(x).getX();
			int y1 = points.get(x).getY();
			int x2 = points.get(x + 1).getX();
			int y2 = points.get(x + 1).getY();

			g2d.drawLine(x1, y1, x2, y2);
		}
		repaint();
	}

	public boolean isDrawingEnabled() {
		return drawingEnabled;
	}

	public void setDrawingEnabled(boolean enabled) {
		drawingEnabled = enabled;
	}
}
