package info.owczarek.ssr.kalambury.klient.gui;

import java.awt.event.*;
import javax.swing.*;
import org.apache.logging.log4j.*;

public class ConnectionPanel extends JPanel {
	private GUI gui;
	private JTextField tfServerIP;
	private JTextField tfServerPort;
	private JTextField tfUserName;
	private JButton bLogIn;
	private static Logger logger = LogManager.getLogger("GUI ConnectionPanel");

	public ConnectionPanel(GUI gui) {
		this.gui = gui;
		createElements();
		deployElements();
		setDefaultValuesInFields();
		connectListeners();
	}

	private void createElements() {
		tfServerIP = new JTextField(15);
		tfServerPort = new JTextField(15);
		tfUserName = new JTextField(15);
		bLogIn = new JButton("Zaloguj");
	}

	private void deployElements() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		JPanel pHost = new JPanel();
		JPanel pPort = new JPanel();
		JPanel pUserName = new JPanel();

		pHost.add(new JLabel("Host:"));
		pPort.add(new JLabel("Port:"));
		pUserName.add(new JLabel("Nazwa użytkownika"));

		pHost.add(tfServerIP);
		pPort.add(tfServerPort);
		pUserName.add(tfUserName);

		add(pHost);
		add(pPort);
		add(pUserName);
		add(bLogIn);
	}
	
	/**
	 * Sets some default values in fields so that it wouldn't be necessary to set them
	 * manually whenever application is tested
	 */
	private void setDefaultValuesInFields() {
		tfServerIP.setText("127.0.0.1");
		tfServerPort.setText("1050");
		tfUserName.setText("Janek");
	}

	private void connectListeners() {
		bLogIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.debug("bLogIn button clicked");
				aLogIn();
			}
		});
	}

	public String getUserName() {
		return tfUserName.getText();
	}

	private String getServerIP() {
		return tfServerIP.getText();
	}

	private int getServerPort() {
		return Integer.valueOf(tfServerPort.getText());
	}

	private void aLogIn() {
		boolean loggingInSucceeded = gui.logIn(getServerIP(), getServerPort(),
				getUserName());
		if (loggingInSucceeded) {
			gui.aShowPaintPanel();
		} else {
			logger.error("Logging in on {}:{} with name {} failed",
					getServerIP(), getServerPort(), getUserName());
			JOptionPane
					.showMessageDialog(
							null,
							"Nie udało się zalogować. Prawdopodobnie nazwa użytkownika jest już zajęta.",
							"Błąd logowania", JOptionPane.ERROR_MESSAGE);
		}
	}
}
