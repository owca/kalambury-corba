package info.owczarek.ssr.kalambury.klient.gui.utils;

/**
 * It represents some point. It was thought to represent mouse coordinates
 */
public class Point {
	private int x;
	private int y;

	/**
	 * Creates new point with specified coordinates
	 * @param x coordinate X
	 * @param y coordinate Y
	 */
	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
