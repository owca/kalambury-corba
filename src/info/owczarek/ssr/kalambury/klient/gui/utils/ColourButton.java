package info.owczarek.ssr.kalambury.klient.gui.utils;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ColourButton extends JButton implements ActionListener {
	private ColorSwitching cs;
	private int rgb;
	
	/**
	 * Creates new set colour button
	 * @param rgb determines its colour
	 * @param cs which component colour to change (Paint)
	 */
	public ColourButton(String colourName, int rgb, ColorSwitching cs) {
		super(colourName);
		setForeground(new Color(rgb));
		this.cs = cs;
		this.rgb = rgb;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		cs.setColour(rgb);
	}
}
