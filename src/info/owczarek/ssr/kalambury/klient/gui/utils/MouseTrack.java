package info.owczarek.ssr.kalambury.klient.gui.utils;

import java.awt.Color;
import java.util.*;

/**
 * This class is responsible for tracing mouse track. It contains some colour
 * and succesive mouse coordinates
 */
public class MouseTrack {
	private int rgb;
	private List<Point> coordinates;

	/**
	 * Creates new path with specified colour
	 * 
	 * @param rgb
	 *            Specifies the colour of the path
	 */
	public MouseTrack(int rgb) {
		this.rgb = rgb;
		coordinates = new ArrayList<Point>();
	}

	/**
	 * Creates MouseTrack converted into an array
	 * 
	 * @param trackArray
	 *            array containing succesive coordinates and colour
	 */
	public MouseTrack(int[] trackArray) {
		this.rgb = trackArray[0];
		coordinates = new ArrayList<Point>();

		for (int i = 0; i < trackArray.length / 2; i++) {
			int x = trackArray[(i * 2) + 1];
			int y = trackArray[(i * 2) + 2];
			Point p = new Point(x, y);
			coordinates.add(p);
		}
	}

	/**
	 * Every time the mouse moves it adds another coordinate
	 */
	public void addCoodrinate(Point p) {
		coordinates.add(p);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("rgb: " + rgb);

		for (Point p : coordinates) {
			sb.append(p);
		}

		return sb.toString();
	}

	/**
	 * Gives the path's colour in rgb
	 * 
	 * @return path's colour in rbg
	 */
	public int getRGB() {
		return rgb;
	}

	public Color getColor() {
		return new Color(getRGB());
	}

	/**
	 * Gives a list of path coordiates
	 * 
	 * @return list of succesive points
	 */
	public List<Point> getCoordinates() {
		return coordinates;
	}

	/**
	 * Converts it into an array possible to send via network
	 * 
	 * @return
	 */
	public int[] toArray() {
		// Creates an array to store colour and coordinates
		int[] result = new int[coordinates.size() * 2 + 1];
		result[0] = rgb;
		for (int x = 0; x < coordinates.size(); x++) {
			result[(x * 2) + 1] = coordinates.get(x).getX();
			result[(x * 2) + 2] = coordinates.get(x).getY();
		}
		return result;
	}
}
