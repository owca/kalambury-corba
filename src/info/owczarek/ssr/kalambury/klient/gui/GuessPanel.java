package info.owczarek.ssr.kalambury.klient.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.apache.logging.log4j.*;

import javax.swing.*;

public class GuessPanel extends JPanel {
	private JLabel lCategory;
	private JTextArea taCommunicationArea;
	private JTextField tfGuessField;
	private JButton bGuess;
	private GUI gui;
	private static Logger logger = LogManager.getLogger("GuessPanel");
	// Administration fields
	private JPanel administrationPanel;
	private JSpinner sTurns;
	private JButton bStartGame;

	public GuessPanel(GUI gui) {
		this.gui = gui;
		createElements();
		deployElements();
		connectListeners();
	}

	private void createElements() {
		lCategory = new JLabel("Kategoria: ");
		tfGuessField = new JTextField();
		taCommunicationArea = new JTextArea(5, 50);
		taCommunicationArea.setEditable(false);
		bGuess = new JButton("Zgaduj");
		administrationPanel = new JPanel();
		sTurns = new JSpinner(new SpinnerNumberModel(5, 1, 10, 1));
		bStartGame = new JButton("Rozpocznij grę");
	}

	private void deployElements() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		tfGuessField.setMaximumSize(new Dimension(400, 20));

		administrationPanel.add(new JLabel("Ilośc rund"));
		administrationPanel.add(sTurns);
		administrationPanel.add(bStartGame);
		administrationPanel.setVisible(false);
		add(administrationPanel);
		add(lCategory);
		add(new JScrollPane(taCommunicationArea));
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.LINE_AXIS));
		p.add(tfGuessField);
		p.add(bGuess);
		add(p);
	}

	private void connectListeners() {
		bGuess.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				aTryToGuess();
			}
		});
		bStartGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				aStartGame();
			}
		});
	}

	private void aStartGame() {
		int numberOfPlayers = gui.getNumberOfPlayers();
		logger.debug("Guessing", numberOfPlayers);
		if (numberOfPlayers > 1) {
			gui.aStartGame((Integer) sTurns.getValue());
			administrationPanel.setVisible(false);
		} else {
			JOptionPane
					.showMessageDialog(
							this,
							"Nie można rozpocząć gry z powodu zbyt małej liczby graczy",
							"Za mało graczy", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void aTryToGuess() {
		gui.aTryToGuess(tfGuessField.getText());
		tfGuessField.setText("");
	}

	public void showAdminPanel() {
		administrationPanel.setVisible(true);
	}

	public void receiveCategoryName(String categoryName) {
		lCategory.setText("Kategoria: " + categoryName);
	}

	public void receivePhraseToDraw(String phrase) {
		lCategory.setText("Do narysowania: \"" + phrase + "\"");
	}

	public void block() {
		tfGuessField.setEditable(false);
		tfGuessField.setText("");
		bGuess.setEnabled(false);
	}

	public void unblock() {
		tfGuessField.setEditable(true);
		tfGuessField.setText("");
		bGuess.setEnabled(true);
	}

	public void addMessage(String message) {
		taCommunicationArea.setText(taCommunicationArea.getText() + message
				+ "\n");
	}
}
