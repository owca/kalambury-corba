package info.owczarek.ssr.kalambury.klient.gui;

import info.owczarek.ssr.kalambury.klient.Configuration;
import info.owczarek.ssr.kalambury.klient.gui.utils.ColourButton;
import info.owczarek.ssr.kalambury.klient.gui.utils.MouseTrack;
import info.owczarek.ssr.kalambury.klient.gui.utils.PaintListener;
import org.apache.logging.log4j.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.*;

import javax.swing.*;

/**
 * This component is responsible for drawing. It allows to paint on it using
 */
public class PaintPanel extends JPanel implements PaintListener {
	private boolean isEnabled = true;
	private PaintingCanvas paintingCanvas;
	private JPanel buttonsPanel;
	private List<ColourButton> buttons;
	private GUI gui;
	private Logger logger = LogManager.getLogger("Paint Panel");

	public PaintPanel(GUI gui) {
		this.gui = gui;
		
		createComponents();
		deployComponents();
		connectListeners();
	}

	private void createComponents() {
		paintingCanvas = new PaintingCanvas();
		paintingCanvas.setSize(640, 480);
		paintingCanvas.setBackground(Color.WHITE);

		// Create colour buttons
		buttonsPanel = new JPanel();
		buttonsPanel
				.setLayout(new BoxLayout(buttonsPanel, BoxLayout.PAGE_AXIS));
		
		buttons = new LinkedList<>();
		Iterator<String> availableColoursIterator = Configuration.COLOURS
				.keySet().iterator();
		while (availableColoursIterator.hasNext()) {
			String nextColour = availableColoursIterator.next();
			ColourButton cb = new ColourButton(nextColour,
					Configuration.COLOURS.get(nextColour), paintingCanvas);
			buttonsPanel.add(cb);
			buttons.add(cb);
		}
	}

	private void deployComponents() {
		setLayout(new BorderLayout());
		add(buttonsPanel, BorderLayout.LINE_START);
		add(paintingCanvas, BorderLayout.CENTER);
	}
	
	private void connectListeners() {
		paintingCanvas.addPaintListener(this);
	}
	
	public void block() {
		for (ColourButton cb: buttons) {
			cb.setEnabled(false);
		}
		paintingCanvas.setDrawingEnabled(false);
	}

	public void unblock() {
		for (ColourButton cb: buttons) {
			cb.setEnabled(true);
		}
		paintingCanvas.setDrawingEnabled(true);
	}

	@Override
	public void aReceiveMouseTrack(MouseTrack mt) {
		logger.debug("Paint Panel received new mouse track");
		gui.aSendMouseTrack(mt);
	}

	public void receiveDrawingPath(MouseTrack mt) {
		paintingCanvas.drawMouseTrack(mt);
	}
}
