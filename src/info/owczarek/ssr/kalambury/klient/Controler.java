package info.owczarek.ssr.kalambury.klient;

import info.owczarek.ssr.kalambury.klient.gui.GUI;
import info.owczarek.ssr.kalambury.klient.gui.utils.MouseTrack;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.AlreadyBound;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;
import kalambury.*;
import org.apache.logging.log4j.*;

public class Controler {
	private ClientImpl client;
	private NamingContextExt nc;
	private Server server;
	private ORB orb;
	private GUI gui;
	private boolean loggedIn;
	private static Logger logger = LogManager.getLogger("Controler");
	private String userName;

	public Controler(GUI gui) {
		this.gui = gui;
		gui.connectControler(this);
	}

	public boolean logIn(String host, int port, String userName) {
		POA rootpoa;
		try {
			// Set some necessary properties and initiate ORB
			Properties props = new Properties();
			props.put("org.omg.CORBA.ORBInitialPort", port);
			props.put("org.omg.CORBA.ORBInitialHost", host);

			orb = ORB.init(new String[] { "-ORBInitialPort", "" + port,
					"-ORBInitialHost", host }, props);

			// Find and activate root POA
			rootpoa = POAHelper.narrow(orb
					.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();

			// Create new client inicialization and create it's reference
			client = new ClientImpl(this);
			org.omg.CORBA.Object clientRef = rootpoa
					.servant_to_reference(client);
			Client klientHref = ClientHelper.narrow(clientRef);

			// Find reference to NameService
			org.omg.CORBA.Object objRef = orb
					.resolve_initial_references("NameService");
			nc = NamingContextExtHelper.narrow(objRef);

			// Bind new client under specified name
			NameComponent path[] = nc.to_name(userName);
			nc.bind(path, klientHref);

			// Fetch server's reference
			server = ServerHelper
					.narrow(nc
							.resolve_str(info.owczarek.ssr.kalambury.server.ServerImpl.SERVER_NAME));

			// Start orb using new thread
			ORBThread orbt = new ORBThread(orb);
			orbt.start();

			loggedIn = server.logIn(userName);
			this.userName = userName;
			this.loggedIn = true;

			return true;
		} catch (InvalidName e) {
			e.printStackTrace();
		} catch (AdapterInactive e) {
			e.printStackTrace();
		} catch (ServantNotActive e) {
			e.printStackTrace();
		} catch (WrongPolicy e) {
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			e.printStackTrace();
		} catch (NotFound e) {
			e.printStackTrace();
		} catch (CannotProceed e) {
			e.printStackTrace();
		} catch (AlreadyBound e) {
			e.printStackTrace();
		}
		this.loggedIn = false;
		return false;
	}

	private class ORBThread extends Thread implements Runnable {
		private ORB orb;

		public ORBThread(ORB orb) {
			this.orb = orb;
		}

		@Override
		public void run() {
			orb.run();
		}
	}

	public void newUserJoined(String newUserName) {
		gui.newUserJoined(newUserName);
	}

	public void userLeft(String oldUser) {
		gui.userLeft(oldUser);
	}

	public void youreTheAdmin() {
		logger.info("You're the admin");
		gui.youreTheAdmin();
	}

	public void newDrawing(String categoryName, String drawer) {
		gui.newDrawing(categoryName, drawer);
	}

	public void receiveDrawingPath(int[] path) {
		MouseTrack mt = new MouseTrack(path);
		gui.receiveDrawingPath(mt);
	}
	
	public void sendMouseTrack(MouseTrack mt) {
		logger.info("Path sent to server");
		int[] path = mt.toArray();
		logger.debug("Path " + path);
		server.draw(path);
	}

	public void userGuessed(String user) {
		// TODO Auto-generated method stub

	}

	public void logOut() {
		if (loggedIn) {
			server.logout(userName);
			NameComponent path[];
			try {
				path = nc.to_name(userName);
				nc.unbind(path);
			} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
				e.printStackTrace();
			} catch (NotFound e) {
				e.printStackTrace();
			} catch (CannotProceed e) {
				e.printStackTrace();
			}
		}
	}

	public void aTryToGuess(String guess) {
		logger.info("Próba odgadnięcia hasła: " + guess);
		server.tryToGuess(guess, userName);
	}

	public void aStartGame(int numberOfTurns) {
		logger.info("Uruchomienie gry przez administratora");
		server.start(numberOfTurns);
	}

	public void startDrawing(String phrase) {
		gui.startDrawing(phrase);
	}

	public void aSendMouseTrack(MouseTrack mt) {
		server.draw(mt.toArray());
	}

	public void guessTrial(String player, String guess, boolean wasSuccessful) {
		logger.info("Próba odgadnięcia przez gracza " + player + ": " + guess);
		gui.guessTrial(player, guess, wasSuccessful);
	}
}
