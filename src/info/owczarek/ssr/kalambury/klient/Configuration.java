package info.owczarek.ssr.kalambury.klient;

import java.util.HashMap;
import java.util.Map;

public class Configuration {
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	public static final String TITLE = "Kalambury";
	
	public static final Map<String, Integer> COLOURS = new HashMap();
	
	static {
		COLOURS.put("Czerwony", 0xff0000);
		COLOURS.put("Zielony", 0x00ff00);
		COLOURS.put("Niebieski", 0x0000ff);
		COLOURS.put("Żółty", 0xffff00);
		COLOURS.put("Różowy", 0xff00ff);
		COLOURS.put("Turkusowy", 0x00ffff);
		COLOURS.put("Czarny", 0x0);
		COLOURS.put("Biały", 0xffffff);
		COLOURS.put("Szary", 0xaaaaaa);
	}
}
