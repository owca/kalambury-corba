package info.owczarek.ssr.kalambury.klient;

import info.owczarek.ssr.kalambury.klient.gui.GUI;
import javax.swing.SwingUtilities;

public class MainClient {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GUI g = new GUI();
				Controler c = new Controler(g);
				g.show();
			}
		});
	}
}
