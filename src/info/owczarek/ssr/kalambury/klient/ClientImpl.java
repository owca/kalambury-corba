package info.owczarek.ssr.kalambury.klient;

import kalambury.ClientPOA;
import org.apache.logging.log4j.*;

public class ClientImpl extends ClientPOA {
	private Controler controler;
	private static Logger logger = LogManager.getLogger("ClientImpl");

	public ClientImpl(Controler controler) {
		this.controler = controler;
		logger.debug("ClientImpl created");
	}

	/**
	 * New user joined the game
	 */
	@Override
	public void newUserJoined(String newUserName) {
		logger.entry();
		logger.info("New user joined " + newUserName);
		controler.newUserJoined(newUserName);
		logger.exit();
	}

	/**
	 * Some user left the game
	 */
	@Override
	public void userLeft(String oldUser) {
		logger.entry();
		logger.info("User left: " + oldUser);
		controler.userLeft(oldUser);
		logger.exit();
	}

	/**
	 * The current player was the first one and therefore is the admin
	 */
	@Override
	public void youreTheAdmin() {
		logger.entry();
		controler.youreTheAdmin();
		logger.exit();
	}

	/**
	 * Some player starts to draw a phase from some category
	 */
	@Override
	public void newTurn(String categoryName, String drawer) {
		logger.entry();
		controler.newDrawing(categoryName, drawer);
		logger.exit();
	}

	/**
	 * The player who draws has sent the last path he drawed
	 */
	@Override
	public void receiveDrawingPath(int[] path) {
		logger.entry();
		controler.receiveDrawingPath(path);
		logger.exit();
	}

	/**
	 * Some user guessed the correct phrase so we add him point
	 */
	@Override
	public void userGuessed(String user) {
		logger.entry();
		controler.userGuessed(user);
		logger.exit();
	}

	/**
	 * The current player has to draw some specified phrase
	 */
	@Override
	public void startDrawing(String phrase) {
		logger.entry();
		logger.info("New drawing of phrase {}", phrase);
		controler.startDrawing(phrase);
		logger.exit();
	}

	/**
	 * Some player tried to guess the phrase
	 */
	@Override
	public void guessTrial(String player, String guess, boolean wasSuccessful) {
		controler.guessTrial(player, guess, wasSuccessful);
	}
}
