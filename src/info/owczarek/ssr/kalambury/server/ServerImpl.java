package info.owczarek.ssr.kalambury.server;

import java.util.*;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextPackage.*;
import org.apache.logging.log4j.*;
import kalambury.*;

public class ServerImpl extends kalambury.ServerPOA {
	private static Logger logger = LogManager.getLogger("ServerImpl");
	public static final String SERVER_NAME = "kalambury_server";
	public static final String SERVER_HOST = "127.0.0.1";
	public static final int SERVER_PORT = 1050;
	private ORB orb;
	private NamingContextExt context;
	private Map<String, Client> players;
	private Game game;

	public ServerImpl(ORB orb, NamingContextExt context) {
		logger.entry();
		this.orb = orb;
		this.context = context;
		players = new HashMap();
		logger.debug("New ServerImpl created");
		logger.exit();
	}

	@Override
	public boolean logIn(String userName) {
		logger.entry();
		logger.info("User " + userName + " comes");
//		if (game != null && !game.isOn()) {
			Client newClient = connectWithNewClient(userName);
			if (newClient != null) {
				// If this is the first user tell him he's an administrator
				if (players.size() == 1) {
					newClient.youreTheAdmin();
					game = new Game(this);
					logger.info("User " + userName + " is the admin");
				} else {
					sendNamesOfAllPlayers(userName);
				}
				game.addPlayer(userName);
			}

			// tell everybody about newcomer
			Iterator<String> playersIterator = players.keySet().iterator();
			while (playersIterator.hasNext()) {
				String playerName = playersIterator.next();
				players.get(playerName).newUserJoined(userName);
			}

			logger.info(getListOfLoggedUsers());
			return logger.exit(true);
//		} else {
//			return logger.exit(false);
//		}
	}

	private String getListOfLoggedUsers() {
		logger.entry();
		StringBuilder sb = new StringBuilder();

		Iterator<String> client = players.keySet().iterator();
		while (client.hasNext()) {
			sb.append(client.next() + "\n");
		}

		return logger.exit(sb.toString());
	}

	/**
	 * Tries to connect new user to the game.
	 * 
	 * @param userName
	 *            new potential user name
	 * @return reference to new client or full if operation failed
	 */
	private Client connectWithNewClient(String userName) {
		logger.entry();
		if (!players.containsKey(userName)) {
			Client newClient = null;
			try {
				newClient = ClientHelper.narrow(context.resolve_str(userName));
			} catch (NotFound e) {
				e.printStackTrace();
			} catch (CannotProceed e) {
				e.printStackTrace();
			} catch (InvalidName e) {
				e.printStackTrace();
			}
			players.put(userName, newClient);
			return logger.exit(newClient);
		} else {
			return logger.exit(null);
		}
	}

	/**
	 * Sends names of all players to some player
	 * 
	 * @param userName
	 *            which player should receive names
	 */
	private void sendNamesOfAllPlayers(String userName) {
		logger.entry();
		Client client = players.get(userName);
		Iterator<String> playersIterator = players.keySet().iterator();
		while (playersIterator.hasNext()) {
			String nextPlayer = playersIterator.next();
			if (!nextPlayer.equals(userName)) {
				client.newUserJoined(nextPlayer);
			}
		}
		logger.info("Names of all players has been sent to {}", userName);
		logger.exit();
	}

	@Override
	public void logout(String userName) {
		logger.entry();
		logger.info("Player {} leaves", userName);
		players.remove(userName);
		// notify all players
		Iterator<Client> playersIterator = players.values().iterator();
		while (playersIterator.hasNext()) {
			Client player = playersIterator.next();
			player.userLeft(userName);
		}
		game.removePlayer(userName);
		logger.exit();
	}

	@Override
	public void start(int turns) {
		logger.entry();
		logger.info("Start new game with {} turns", turns);
		game.setNumberOfTurns(turns);
		game.start();
		logger.exit();
	}

	@Override
	public void draw(int[] path) {
		logger.entry();
		logger.debug("Path received " + path);
		// send drawing path to all the players
		Iterator<Client> playersIterator = players.values().iterator();
		while (playersIterator.hasNext()) {
			Client player = playersIterator.next();
			player.receiveDrawingPath(path);
		}
		logger.exit();
	}

	@Override
	public void tryToGuess(String guess, String user) {
		logger.entry();
		game.tryToGuess(user, guess);
		// wyślij do wszystkich
		logger.exit();
	}

	public void newDrawing(String drawerName, String phrase, String hint) {
		Client currentDrawer = players.get(drawerName);
		currentDrawer.startDrawing(phrase);

		Iterator<Client> playersIterator = players.values().iterator();
		while (playersIterator.hasNext()) {
			Client player = playersIterator.next();
			if (player != currentDrawer) {
				player.newTurn(hint, drawerName);
			}
		}
	}

	public void userGuessed(String userName) {
		Iterator<Client> playersIterator = players.values().iterator();
		while (playersIterator.hasNext()) {
			Client player = playersIterator.next();
			player.userGuessed(userName);
		}
	}

}
