package info.owczarek.ssr.kalambury.server;

import java.util.Properties;
import org.apache.logging.log4j.*;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POAManagerPackage.*;
import org.omg.PortableServer.POAPackage.*;
import kalambury.*;

public class MainServer {
	private static Logger logger = LogManager.getLogger("Server Main");

	public static void main(String[] args) {
		logger.info("Starting server at {}:{}", ServerImpl.SERVER_HOST,
				ServerImpl.SERVER_PORT);
		Properties props = new Properties();
		props.put("org.omg.CORBA.ORBInitialPort", ServerImpl.SERVER_PORT);
		props.put("org.omg.CORBA.ORBInitialHost", ServerImpl.SERVER_HOST);
		ORB orb = ORB.init(new String[] { "-ORBInitialPort",
				"" + ServerImpl.SERVER_PORT, "-ORBInitialHost",
				ServerImpl.SERVER_HOST }, props);
		logger.info("ORB initiated");

		try {
			// Pobieranie referencji do rootpoa i aktywowanie POAManagera
			POA rootpoa = POAHelper.narrow(orb
					.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();

			// Pobieranie referencji do usługi nazw
			org.omg.CORBA.Object ncObjectRef = orb
					.resolve_initial_references("NameService");
			NamingContextExt ncRef = NamingContextExtHelper.narrow(ncObjectRef);

			ServerImpl serwer = new ServerImpl(orb, ncRef);
			logger.info("Server implementation created");

			org.omg.CORBA.Object serwerReferencja = rootpoa
					.servant_to_reference(serwer);
			Server serwerHref = ServerHelper.narrow(serwerReferencja);

			NameComponent path[] = ncRef.to_name(ServerImpl.SERVER_NAME);
			ncRef.rebind(path, serwerHref);
			logger.info("Server binded under name " + ServerImpl.SERVER_NAME);

			logger.info("Server is waiting for coming players");
			orb.run();
			logger.info("Server is down");

		} catch (InvalidName e) {
			e.printStackTrace();
		} catch (AdapterInactive e) {
			e.printStackTrace();
		} catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
			e.printStackTrace();
		} catch (ServantNotActive e) {
			e.printStackTrace();
		} catch (WrongPolicy e) {
			e.printStackTrace();
		} catch (NotFound e) {
			e.printStackTrace();
		} catch (CannotProceed e) {
			e.printStackTrace();
		}
	}
}
