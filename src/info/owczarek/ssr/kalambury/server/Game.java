package info.owczarek.ssr.kalambury.server;

import java.util.*;

import org.apache.logging.log4j.*;

public class Game {
	private int turns;
	private int currentTurn;
	private Map<String, Integer> scores;
	private int playersLeftInTurn;
	private Iterator<String> currentPlayerIterator;
	private Logger logger = LogManager.getLogger("Game");
	private ServerImpl server;
	private boolean isOn;
	private String currentPhrase;
	private String currentPhraseHint;
	private static String[][] phrases = {
			{ "Przysłowia", "Gdzie dwóch się bije tam trzeci korzysta",
					"Gdzie kucharek sześć tam nie ma co jeść" },
			{ "Państwa", "Anglia", "Polska", "Argentyna", "Niemcy",
					"Stany Zjednoczone" } };
	private static Random ran = new Random(System.currentTimeMillis());

	public Game(ServerImpl server) {
		this.server = server;
		isOn = false;
		currentTurn = 0;
		scores = new HashMap<String, Integer>();
	}

	public void setNumberOfTurns(int turns) {
		logger.info("Number of turns set to {}", turns);
		this.turns = turns;
	}

	public void addPlayer(String playerName) {
		logger.info("Player {} joined", playerName);
		scores.put(playerName, Integer.valueOf(0));
	}

	public void removePlayer(String playerName) {
		logger.info("Player {} left", playerName);
		scores.remove(playerName);
	}

	public void nextTurn() {
		logger.info("Next turn");
		currentTurn++;
		currentPlayerIterator = scores.keySet().iterator();
		playersLeftInTurn = scores.size();
	}

	public void nextDrawing() {
		if (currentPlayerIterator.hasNext()) {
			String nextDrawer = currentPlayerIterator.next();
			playersLeftInTurn--;
			randomizePhrase();
			server.newDrawing(nextDrawer, currentPhrase, currentPhraseHint);
		} else {
			if (currentTurn < turns) {
				nextTurn();
			} else {
				endOfGame();
			}
		}
	}

	/**
	 * Some user claims to know the correct answer
	 * 
	 * @param userName
	 *            which user
	 * @param guess
	 *            his guess
	 * @return user guessed correctly or not
	 */
	public void tryToGuess(String userName, String guess) {
		if (currentPhrase.toLowerCase().equals(guess.toLowerCase())) {
			logger.info("User {} guessed", userName);
			server.userGuessed(userName);
			nextDrawing();
		}
		logger.info("User {} failed to guess with {}", userName, guess);
	}

	/**
	 * Adds one point whenever player guessed
	 * 
	 * @param userName
	 *            which player guessed
	 */
	private void addPoints(String userName) {
		logger.debug("Adding point for {}", userName);
		scores.put(userName, scores.get(userName) + 1);
	}

	/**
	 * Starts the whole game
	 */
	public void start() {
		isOn = true;
		nextTurn();
		nextDrawing();
	}
	
	public boolean isOn() {
		return isOn();
	}

	private void endOfGame() {
		// TODO Auto-generated method stub

	}

	/**
	 * Randomizes category and phrase for drawing
	 */
	private void randomizePhrase() {
		int categoryIndex = ran.nextInt(phrases.length);
		currentPhraseHint = phrases[categoryIndex][0];
		
		int numberOfPhrasesInCategory = phrases[categoryIndex].length - 1;
		int phraseIndex = 1 + ran.nextInt(numberOfPhrasesInCategory);
		currentPhrase = phrases[categoryIndex][phraseIndex];
		logger.info("Randomized phrase {} from {}", currentPhrase, currentPhraseHint);
	}
}
